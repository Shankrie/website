$(document).ready(function() {

	const LANG_LIT_SYMBOL = "LIT";
	const LANG_ENG_SYMBOL = "ENG";
	const GALLERY_ID = '#gallery';
	const HIDDEN_GALLERY_ID = '#hidden-gallery';

	var popupHeight = 60;
	var menuIsActive = false;
	var langDropDownIsActive = false;
	
	$(window).resize(function() {
		if (menuIsActive && document.documentElement.clientWidth > 580) {
			popupHeight = 60;

			$('.main').css({
				'margin-top': '60' + 'px'
			})

			$('#header').css({
				'position': 'fixed'
			});
		}else if (menuIsActive && document.documentElement.clientWidth <= 580){
			popupHeight = $('#popup-menu').height() + 60;

			$('.main').css({
				'margin-top': popupHeight + 'px'
			})
		}
	});

	var videoWidth = $('iframe').width();
	$('iframe').css({
		'height': 'calc(' + videoWidth + 'px * 0.5625)'
	});

	$('.more').click(function() {
		$('#hidden-gallery').slideToggle();
	});


	function scrollToAbout(){
		$("body, html").stop().animate({scrollTop: 0});
		return false;
	};
	
	function scrollToProjects(){
		$("body, html").stop().animate({scrollTop: $(".main_services").offset().top - 60});
		return false;
	};
	
	function scrollToServices(){
		$("body, html").stop().animate({scrollTop: $(".main_projects").offset().top - 60});
		return false;
	};
	
	function scrollToContacts(){
		$("body, html").stop().animate({scrollTop: $(".main_contacts").offset().top - 60});
		return false;
	};
	
	$('.menu-apie-mus').click(function() {
		window.requestAnimationFrame(scrollToAbout);
	});
	$('.menu-paslaugos').click(function() {
		window.requestAnimationFrame(scrollToProjects);
	});
	$('.menu-projektai').click(function() {
		window.requestAnimationFrame(scrollToServices);
		/*$("body, html").stop().animate({scrollTop: $(".main_projects").offset().top - popupHeight});
		return false;*/
	});
	$('.menu-kontaktai').click(function() {
		window.requestAnimationFrame(scrollToContacts);
	});
	$('#button').click(function() {
		$("body, html").stop().animate({scrollTop: $(".main_projects").offset().top - popupHeight});
		if (menuIsActive) {
			$('#popup-menu').css({
				'visibility': 'hidden'
			});
			menuIsActive = false;
		}
		return false;
	});

	var checkEnabled = true;
	var checkFinished = false;

	var img;
	var imageThumbnails = new Array();
    var videoThumbnails = new Array();
	var i = 0;

	var myInterval = setInterval(loadImages, 1);

	function loadImages() {

		if (checkFinished) {
			clearInterval(myInterval);
			appendThumbnails();
			return;
		}

		if (checkEnabled) {
			checkEnabled = false;

			img = new Image();
			img.onload = addImage;
			img.onerror = finishCheck;
			img.src = 'images/project' + i + '.jpg';
		}

	}

	function addImage() {
		imageThumbnails.push(img);
		i++;
		checkEnabled = true;
	}

	function finishCheck() {
		checkFinished = true;
	}

	function appendThumbnails() {
		var thumbLimit = 12;

		for (var i = 0; i < imageThumbnails.length; i++) {
			var thumbnail = document.createElement('div');
			thumbnail.style.backgroundImage = 'url(images/project' + i + '.jpg)';
			thumbnail.className = 'thumbnail';
			thumbnail.id = 'thumb' + i;
			$(GALLERY_ID).append(thumbnail);
		}
	}

    function appendImage() {
        var thumbnail = document.createElement('div');
        thumbnail.style.backgroundImage = 'url(images/thumb_project' + i + '.jpg)';
        thumbnail.className = 'thumbnail';
        thumbnail.id = 'thumb' + i;
        $(GALLERY_ID).append(thumbnail);
    }

	preloadPictures();
	function preloadPictures() {
		var image = new Image();
		image.src = 'images/project7.jpg';
	}
	
	var pictureIndex = 0;

	$('#gallery').on('click', '.thumbnail', function(e) {
		pictureIndex = e.target.id.substring(5);
		document.getElementById("gallery-pic").setAttribute("src", imageThumbnails[parseInt(pictureIndex)].src);

		if (pictureIndex != 0)
			showLeftChevron();
		if (pictureIndex != imageThumbnails.length-1)
			showRightChevron();

		$('#gallery-zoom').css({
			'visibility': 'visible'
		});

		$('#page-fade').css({
			'visibility': 'visible'
		});
	});
	
	function hideRightChevron() {
		$('#chevron-right').css({
			'visibility': 'hidden'
		});
	}
	
	function hideLeftChevron() {
		$('#chevron-left').css({
			'visibility': 'hidden'
		});
	}
	
	function showRightChevron() {
		$('#chevron-right').css({
			'visibility': 'visible'
		});
	}
	
	function showLeftChevron() {
		$('#chevron-left').css({
			'visibility': 'visible'
		});
	}
	
	$('#gallery-zoom').click(function(e) {
		if (e.target != $('#gallery-pic')[0] && e.target != $('#chevron-left')[0] && e.target != $('#chevron-right')[0]) {
			$('#gallery-zoom').css({
				'visibility': 'hidden'
			});
			$('#page-fade').css({
				'visibility': 'hidden'
			});
			hideLeftChevron()
			hideRightChevron()
		}
		
	});
	
	$('#chevron-right').click(function() {
		if (pictureIndex == imageThumbnails.length - 2) {
			hideRightChevron()
		}else if (pictureIndex == 0) {
			showLeftChevron()
		}
		document.getElementById("gallery-pic").setAttribute("src", imageThumbnails[++pictureIndex].src);
	});
	
	$('#chevron-left').click(function() {
		if (pictureIndex == 1) {
			hideLeftChevron()
		}else if (pictureIndex == imageThumbnails.length-1) {
			showRightChevron()
		}
		document.getElementById("gallery-pic").setAttribute("src", imageThumbnails[--pictureIndex].src);
	});
	
	$('#page-fade').click(function() {
		hideLeftChevron()
		hideRightChevron()
		$('#gallery-zoom').css({
			'visibility': 'hidden'
		});
		$('#page-fade').css({
			'visibility': 'hidden'
		});
		document.getElementById("gallery-pic").setAttribute("src", 'images/black.jpg');
	});
	
	$('#menu-logo').click(function() {
		if (!menuIsActive) {
			$('#popup-menu').css({
				'visibility': 'visible'
			});

			popupHeight = $('#popup-menu').height() + 60;
			
			$('.main').css({
				'margin-top': popupHeight + 'px'
			})

			menuIsActive = true;
		}else {
			$('#popup-menu').css({
				'visibility': 'hidden'
			});
			
			$('.main').css({
				'margin-top': '60px'
			})
			
			menuIsActive = false;
			popupHeight = 60;
		}
	});
	
	$('.popup-menu-link').click(function() {
		$('#popup-menu').css({
			'visibility': 'hidden'
		});
			
		$('.main').css({
			'margin-top': '60px'
		})
			
		menuIsActive = false;
		popupHeight = 60;
	});

	$('#lang-dropdown').click(function() {
		if (langDropDownIsActive) {
			$('#select-items').css({
				'display': 'none'
			});
			langDropDownIsActive = false;
		}
		else {
			$('#select-items').css({
				'display': 'inline'
			});
			langDropDownIsActive = true;
		}
	});

	$(document).on('click', function(event) {
		if (menuIsActive && !$(event.target).closest('#burger-menu').length) {
			// Hide the menus.
			$('#popup-menu').css({
				'visibility': 'hidden'
			});
			
			$('.main').css({
				'margin-top': '60px'
			})
			
			menuIsActive = false;
			popupHeight = 60;
		}

		if (langDropDownIsActive && !$(event.target).closest('#lang-dropdown').length && !$(event.target).closest('#select-items').length) {
			$('#select-items').css({
				'display': 'none'
			});
			langDropDownIsActive = false;
		}
	});
});