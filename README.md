## Description
Single-page responsive website done from scratch in HTML/CSS/JS. 

## Screenshots
![Screenshot](https://drive.google.com/uc?id=0BykSj9RyDkEEcEZQdVc5TzdqMkk)
![Screenshot](https://drive.google.com/uc?id=0BykSj9RyDkEEWU9JNTNvUnNYRms)
![Screenshot](https://drive.google.com/uc?id=0BykSj9RyDkEESmJYMVBrVVREU2M)
![Screenshot](https://drive.google.com/uc?id=0BykSj9RyDkEEcjBNWkdocDBNM00)
![Screenshot](https://drive.google.com/uc?id=0BykSj9RyDkEEZ0pJQVRsbXFROWs)
![Screenshot](https://drive.google.com/uc?id=0BykSj9RyDkEESHl2NEZOVmZ0Z2c)